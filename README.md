# Prueba tecnica

Realizar una API en Javascript con las siguientes tareas:

1. Mostrar un listado de usuarios registrados con la informacion almacenada.

2. Mostrar el usuario por identificador.

3. Agregar usuario, en el cual contiene los siguientes parametros.
- Nombre
- Apellidos
- Numero de documento
- Fecha de nacimiento
- Departamento
- Provincia
- Distrito
- Direccion
- Telefono
- Correo

4. Actualizar usuario, en el cual solicite
- Departamento
- Provincia
- Distrito
- Direccion
- Telefono
- Correo

5. Eliminar usuario por identificador.

Adicionalmente subir el script de la base de datos de la informacion almacenada en el repositorio en una carpeta con el nombre de database. 







